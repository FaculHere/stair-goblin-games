﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour
{
    private float gravityModifier = 1.0f;
    public float projectileStrength = 20.0f;
    private Rigidbody2D projectileRb;

    // Start is called before the first frame update
    void Start()
    {
      
        projectileRb = GetComponent<Rigidbody2D>();
        Physics2D.gravity *= gravityModifier;
        projectileRb.AddForce(Vector3.right * projectileStrength, ForceMode2D.Impulse);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision) 
    {
        if (collision.gameObject.name == "Floor")
        {
            Destroy(gameObject);
        }

        if (collision.gameObject.name == "Wall")
        {
            Destroy(gameObject);
        }

        if (collision.gameObject.name == "Enemy")
        {
            Destroy(gameObject);
        }

    }
}
