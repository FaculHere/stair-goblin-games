﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public float speed = 10.0f;
    public float jumpStrength = 10.0f;
    private float gravityModifier = 1.0f;
    public bool isOnGround = true;
    public GameObject projectileLeft;
    public GameObject projectileRight;
    private Rigidbody2D playerRb;
    private float horizontalInput;

    // Start is called before the first frame update
    void Start()
    {
        playerRb = GetComponent<Rigidbody2D>();
        Physics2D.gravity *= gravityModifier;
    }

    

    // Update is called once per frame
    void Update()
    {
        horizontalInput = Input.GetAxis("Horizontal");

        transform.Translate(Vector3.right * Time.deltaTime * speed * horizontalInput);
        

    
        if (Input.GetKeyDown(KeyCode.W) && isOnGround)
        {
            playerRb.AddForce(Vector3.up * jumpStrength, ForceMode2D.Impulse);
            isOnGround = false;
        }

        if (Input.GetKeyDown(KeyCode.Space) && Input.GetKey(KeyCode.D))
        {
            Instantiate(projectileRight, transform.position + new Vector3(1, 1, 0), projectileRight.transform.rotation);
        }

        if (Input.GetKeyDown(KeyCode.Space) && Input.GetKey(KeyCode.A))
        {
            Instantiate(projectileLeft, transform.position + new Vector3(-1, 1, 0), projectileLeft.transform.rotation);
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Floor")
        {
            isOnGround = true;
        }

        if (collision.gameObject.name == "Enemy")
        {
            Application.LoadLevel(Application.loadedLevel);
        }

    }
}
